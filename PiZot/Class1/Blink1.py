import RPi.GPIO as GPIO
from time import sleep
import random
import os

GPIO.setmode(GPIO.BCM)
BUTTON1=22
BUTTON2=17
BUTTON3=27
GREEN_LED = 23
BLUE_LED = 24
RED_LED = 25

GPIO.setup(BUTTON1, GPIO.IN)
GPIO.setup(BUTTON2, GPIO.IN)
GPIO.setup(BUTTON3, GPIO.IN)
GPIO.setup(GREEN_LED, GPIO.OUT)
GPIO.setup(BLUE_LED, GPIO.OUT)
GPIO.setup(RED_LED, GPIO.OUT)

def led_on(pin):
    GPIO.output(pin, True)
def led_off(pin):
    GPIO.output(pin, False)

green=""
blue=""
red=""
red_on=0
blue_on=0
green_on=0
red_off=0
blue_off=0
green_off=0

for y in range (2):
    print "Loop # %d " % (y)
    #for x in range(23, 26):
    led_on(RED_LED)
    print "LED is ON"
    sleep(.5)
    led_off(RED_LED)
    print "LED is OFF"
    sleep(.5)
#GPIO.cleanup()
#exit()


#print "Before blink"


def blinkLED (pin, times, blinkrate):
    for z in range (times):
        led_on(pin)
        sleep(blinkrate)
        led_off(pin)
        sleep(blinkrate)

blinkLED(RED_LED, 40, .1)

import random
for z in range (6):
    print "LED cycle %d" % (z)
    for x in range (23, 26):
        if (random.randrange(2)):
            led_on(x)
        else:
            led_off(x)            
    sleep((random.randrange(100)+1)/50)

for x in range(23, 26):
    led_off(x)


GPIO.cleanup()
